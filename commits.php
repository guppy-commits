<?php
// By <knittl89@gmail.com>, gry <gry.does.not.check.email@gmail.com>, Kays
// GPL <http://www.gnu.org/licenses/>


function tinyfy ($url) { return $url ? file_get_contents('http://tinyurl.com/api-create.php?url='.$url) : $url;}

$set_my_host = "";
$set_channels_no_color = array ('#wedonotlikecolors','#plaintextfans');
$set_network = 'chat.freenode.net';
$set_channels_to_join = array ('#+n_channel','$another_+n_channel');
$set_channels_color = array ('#color_fans');
$set_nick = 'myprojcommits';
# To have the bot auth to Services  set this to 'servicesaccountname:servicespassword' or just 'servicespassword'
# (This can also be useful at some private servers)
$set_password = '';

// sends a message and waits 1 second before sending the next message
function send($str) {
    socket_write($GLOBALS['socket'], $str . "\r\n");
    sleep(1);
}
// send message to channel
function msg($msg, $channel) {
    send(sprintf('PRIVMSG %s :%s',$channel, str_replace("\r\n",'',$msg)));
}
function abbr($id, $len=7) {
    return substr($id, 0, $len);
}
function shorten_ref($ref, $ns = 'refs/heads/') {
    return preg_replace('#^'.$ns.'#', '', $ref);
}
if(isset($_REQUEST['payload'])) {
    //Connect to IRC.
    $socket = socket_create(AF_INET6, SOCK_STREAM, SOL_TCP);
    socket_bind($socket,$set_my_host);
    echo 'Attempting to connect ...';
    $result = socket_connect($socket, $set_network, 6667);
    if ($result === false) {
        printf("socket_connect() failed.\nReason: (%s)\n", socket_strerror(socket_last_error($socket)));
    } else {
        echo "Connected OK.\n";
    }
    send('user bot a a :A repo.or.cz commits bot');
    send('pass '.$set_password);
    send('nick commits');
    sleep(4); // wait another 4 seconds before joining channels
    // Join channels
    foreach ($set_channels_to_join as $channel){
    send('join '.$channel);
    }
    sleep(1);
    // Get the posted data.
    $data = json_decode($_REQUEST['payload']);


    // No colors version.
foreach ($set_channels_no_color as $channel) {
    // Branch + push ID.
    msg(sprintf('%s pushed %s..%s (%d commit%s) to %s in %s:',
        $data->pusher->name,
        abbr($data->before), abbr($data->after),
        count($data->commits), count($data->commits)!=1?'s':'',
        shorten_ref($data->ref),
        $data->repository->full_name),$channel);
    // Each commit information.
    foreach(array_reverse($data->commits) as $commit) {
        msg(sprintf('* %s %s (%s, %s) %s',
            abbr($commit->id),
            $commit->message,
            $commit->author->name,
            $commit->timestamp,
            tinyfy($commit->url)),$channel);
    }
}

/* 
 * Colored version.
 */

foreach ($set_channels_color as $channel) {

         msg(sprintf('%1$s%2$s:%8$s %3$s%4$s%8$s %5$s%6$s%8$s * %7$s %1$s:%8$s',
             "\x02",
             $data->repository->name,
             "\x0303",
             $data->pusher->name,
             "\x0307",
             shorten_ref($data->ref),
             abbr($data->after),
             "\x0f"
         ),$channel);

# for each commit
     foreach(array_reverse($data->commits) as $commit) {
         preg_match_all('#.{0,100}[^ ]+ ?#', $commit->message, $cmessages, PREG_PATTERN_ORDER);
         
         $count = count($cmessages[0]);

         if (!$count) continue;

         for ($e = 0; $e < $count; $e++) {
             msg(sprintf('%1$s%2$s:%1$s %3$s%4$s',
                 "\x02",
                 $data->repository->name,
                 $cmessages[0][$e],
                 ($e == $count - 1 ? ' - ' . tinyfy($commit->url) : '')
             ),$channel);
         }
     }
}


    // The bot disconnects at end of script.
   send('QUIT');
}







// json dummy data
/*array (
'payload' => '{
  "repository":{
    "owner":{"email":"user@gshellz.org","name":""},
    "pull_url":"git://repo.or.cz/guppy.git",
    "url":"http://repo.or.cz/w/guppy.git",
    "name":"guppy","full_name":"guppy.git"},
  "after":"32080e9a5fe22134c9089d6cbd29150d71f47570",
  "commits":[{"added":[],
             "author":{"email":"user@gshellz.org","name":"user"},
             "modified":["5"],"message":"test commit",
             "timestamp":"2011-05-10 15:30:29",
             "removed":[],
             "url":"http://repo.or.cz/w/guppy.git/commit/32080e9a5fe22134c9089d6cbd29150d71f47570",
             "id":"32080e9a5fe22134c9089d6cbd29150d71f47570"}],
  "pusher":{"email":"user@gshellz.org","name":"user"},
  "ref":"refs/heads/master",
  "before":"f0473eaa9f10d524d2e0d7ece127b10a6840e53a"}',
)*/
?>

